#ifndef TRANSACTIONLIST_H
#define TRANSACTIONLIST_H

#include <QDialog>

namespace Ui {
class transactionList;
}

class transactionList : public QDialog
{
    Q_OBJECT

public:
    explicit transactionList(QWidget *parent = nullptr);
    ~transactionList();

private slots:
    void on_pushButton_clicked();

    void on_tableWidget_cellChanged(int row, int column);

    void on_pushButton_3_clicked();

private:
    Ui::transactionList *ui;
};

#endif // TRANSACTIONLIST_H
