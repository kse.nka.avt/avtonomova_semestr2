#ifndef NEWCLIENT_H
#define NEWCLIENT_H

#include <QDialog>

namespace Ui {
class newClient;
}

class newClient : public QDialog
{
    Q_OBJECT

public:
    explicit newClient(QWidget *parent = nullptr);
    ~newClient();

private slots:
    void on_pushButton_clicked();

private:
    Ui::newClient *ui;
};

#endif // NEWCLIENT_H
