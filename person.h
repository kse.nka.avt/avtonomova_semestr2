#ifndef PERSON_H
#define PERSON_H
#include <string>

class person
{
public:
    person();
    int get_age();
    void set_age(int par);
    std::string get_name();
    void set_name(std::string par);
    std::string get_address();
    void set_address(std::string par);
    std::string get_number();
    void set_number(std::string par);
    std::string get_birth();
    void set_birth(std::string par);
private:
    std::string name;
    std::string address;
    std::string number;
    std::string birth;
    int age;
    std::string login;
};

#endif // PERSON_H
