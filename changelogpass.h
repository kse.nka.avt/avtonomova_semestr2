#ifndef CHANGELOGPASS_H
#define CHANGELOGPASS_H

#include <QDialog>
#include "data.h"

namespace Ui {
class ChangeLogPass;
}

class ChangeLogPass : public QDialog
{
    Q_OBJECT

public:
    explicit ChangeLogPass(QWidget *parent = nullptr, clientStruct *user = nullptr);
    explicit ChangeLogPass(clientStruct *user = nullptr);
    ~ChangeLogPass();
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();

private:
    Ui::ChangeLogPass *ui;
    clientStruct *_user;
};

#endif // CHANGELOGPASS_H
