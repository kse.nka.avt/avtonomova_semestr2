#include "newemployee.h"
#include "ui_newemployee.h"
#include "data.h"

newEmployee::newEmployee(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::newEmployee)
{
    ui->setupUi(this);
}

newEmployee::~newEmployee()
{
    delete ui;
}

void newEmployee::on_pushButton_clicked()
{
    std::string login = ui->lineEdit->text().toUtf8().constData();
    std::string name = ui->lineEdit_2->text().toUtf8().constData();
    std::string address = ui->lineEdit_3->text().toUtf8().constData();
    std::string number = ui->lineEdit_5->text().toUtf8().constData();
    std::string birth = ui->lineEdit_4->text().toUtf8().constData();
    std::string age = ui->lineEdit_7->text().toUtf8().constData();
    std::string job = ui->lineEdit_6->text().toUtf8().constData();
    DataEmployee client_database;
    client_database.add(login + ":" + name + ":" + address + ":" + number + ":" + birth + ":" + age + ":" + job);
    this->hide();
}
