/********************************************************************************
** Form generated from reading UI file 'changepoint.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHANGEPOINT_H
#define UI_CHANGEPOINT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ChangePoint
{
public:
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QLabel *label_4;
    QLineEdit *lineEdit_2;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ChangePoint)
    {
        if (ChangePoint->objectName().isEmpty())
            ChangePoint->setObjectName(QString::fromUtf8("ChangePoint"));
        ChangePoint->resize(550, 323);
        centralWidget = new QWidget(ChangePoint);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(150, 20, 261, 221));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        lineEdit = new QLineEdit(layoutWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout->addWidget(label_4);

        lineEdit_2 = new QLineEdit(layoutWidget);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setEchoMode(QLineEdit::Password);

        verticalLayout->addWidget(lineEdit_2);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        verticalLayout->addItem(horizontalSpacer_9);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);

        ChangePoint->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ChangePoint);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 550, 22));
        ChangePoint->setMenuBar(menuBar);
        mainToolBar = new QToolBar(ChangePoint);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        ChangePoint->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(ChangePoint);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        ChangePoint->setStatusBar(statusBar);

        retranslateUi(ChangePoint);

        QMetaObject::connectSlotsByName(ChangePoint);
    } // setupUi

    void retranslateUi(QMainWindow *ChangePoint)
    {
        ChangePoint->setWindowTitle(QCoreApplication::translate("ChangePoint", "ChangePoint", nullptr));
        label->setText(QCoreApplication::translate("ChangePoint", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt;\">\320\220\320\262\321\202\320\276\321\200\320\270\320\267\320\260\321\206\320\270\321\217</span></p></body></html>", nullptr));
        label_2->setText(QCoreApplication::translate("ChangePoint", "\320\233\320\276\320\263\320\270\320\275:", nullptr));
        label_4->setText(QCoreApplication::translate("ChangePoint", "\320\237\320\260\321\200\320\276\320\273\321\214:", nullptr));
        pushButton->setText(QCoreApplication::translate("ChangePoint", "\320\222\321\205\320\276\320\264", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ChangePoint: public Ui_ChangePoint {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHANGEPOINT_H
