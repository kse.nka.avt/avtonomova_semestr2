#ifndef EMPLOYLIST_H
#define EMPLOYLIST_H

#include <QDialog>
#include <QTableWidgetItem>

namespace Ui {
class employList;
}

class employList : public QDialog
{
    Q_OBJECT

public:
    explicit employList(QWidget *parent = nullptr);
    ~employList();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    //void on_tableWidget_itemChanged(QTableWidgetItem *item);

    void on_tableWidget_cellChanged(int row, int column);

    void on_pushButton_3_clicked();

private:
    Ui::employList *ui;
};

#endif // EMPLOYLIST_H
