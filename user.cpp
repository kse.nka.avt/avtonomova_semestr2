#include "user.h"
#include "ui_user.h"
#include "data.h"
#include <QDebug>
#include "string"
#include "transaction.h"
#include "credit.h"
#include "exchange.h"
#include "changelogpass.h"

User::User(QWidget *parent, clientStruct *the_user) :
    QDialog(parent),
    ui(new Ui::User),
    _user(the_user)
{
    ui->setupUi(this);
}

User::User(clientStruct *the_user) :
    ui(new Ui::User),
    _user(the_user)
{

    ui->setupUi(this);
    ui->label_2->setText(QString::number(_user->balance) + " USD");
}

User::~User()
{
    delete ui;
}

void User::on_pushButton_clicked()
{
    this->hide();
    transaction transaction(_user);
    transaction.setModal(true);
    transaction.exec();
}

void User::on_pushButton_3_clicked()
{
    this->hide();
    credit credit(_user);
    credit.setModal(true);
    credit.exec();
}

void User::on_pushButton_2_clicked()
{
    this->hide();
    exchange exchange(_user);
    exchange.setModal(true);
    exchange.exec();
}

void User::on_pushButton_4_clicked()
{
    this->hide();
    ChangeLogPass change(_user);
    change.setModal(true);
    change.exec();
}
