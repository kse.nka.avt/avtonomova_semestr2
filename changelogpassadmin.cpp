#include "changelogpassadmin.h"
#include "ui_changelogpassadmin.h"
#include "data.h"
#include <QMessageBox>
#include "admin.h"

ChangeLogPassAdmin::ChangeLogPassAdmin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangeLogPassAdmin)
{
    ui->setupUi(this);
}

ChangeLogPassAdmin::ChangeLogPassAdmin(std::string login) :
    ui(new Ui::ChangeLogPassAdmin),
    _login(login)
{
    ui->setupUi(this);
}

ChangeLogPassAdmin::~ChangeLogPassAdmin()
{
    delete ui;
}

void ChangeLogPassAdmin::on_pushButton_2_clicked()
{
    std::string new_login = ui->lineEdit->text().toUtf8().constData();
    std::string new_password = ui->lineEdit_2->text().toUtf8().constData();
    DataLogin login_base;
    int index_login_base = login_base.findFirstIndex("login", _login);
    _login = new_login;
    login_base.change(index_login_base + 1, new_login + ":" + new_password + ":admin");
    QMessageBox::information(this, "succes", "Пароль и логин успешно изменен.");
    this->hide();
    Admin user(_login);
    user.setModal(true);
    user.exec();
}

void ChangeLogPassAdmin::on_pushButton_clicked()
{
    this->hide();
    Admin user(_login);
    user.setModal(true);
    user.exec();
}
