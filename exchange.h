#ifndef EXCHANGE_H
#define EXCHANGE_H

#include <QDialog>
#include "data.h"

namespace Ui {
class exchange;
}

class exchange : public QDialog
{
    Q_OBJECT

public:
    explicit exchange(QWidget *parent = nullptr, clientStruct *user = nullptr);
    explicit exchange(clientStruct *user = nullptr);
    ~exchange();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::exchange *ui;
    clientStruct *_user;
};

#endif // EXCHANGE_H
