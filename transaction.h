#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <QDialog>
#include "data.h"

namespace Ui {
class transaction;
}

class transaction : public QDialog
{
    Q_OBJECT

public:
    explicit transaction(QWidget *parent = nullptr, clientStruct *the_user = nullptr);
    ~transaction();
    explicit transaction(clientStruct *the_user = nullptr);
private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::transaction *ui;
    clientStruct *_user;
    int _index;
};

#endif // TRANSACTION_H
