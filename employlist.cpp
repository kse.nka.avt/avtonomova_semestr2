#include "employlist.h"
#include "ui_employlist.h"
#include "data.h"
#include "admin.h"
#include "newemployee.h"
#include <QDebug>

employList::employList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::employList)
{
    ui->setupUi(this);
    DataEmployee employee_database;
    std::vector<std::vector<std::string>> employees = employee_database.read_all();
    bool oldState = ui->tableWidget->blockSignals(true);

    ui->tableWidget->setRowCount(employees.size());
    ui->tableWidget->setColumnCount(7);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Логин"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Имя"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Адрес"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Номер"));
    ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Дата рождения"));
    ui->tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem("Возраст"));
    ui->tableWidget->setHorizontalHeaderItem(6, new QTableWidgetItem("Должность"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i <= employees.size()-1; i ++){
        for (unsigned int j = 0; j <= employees[0].size()-2; j ++){
            std::string cl = employees[i][j+1];
            item = new QTableWidgetItem(QString(cl.c_str()));
            if (j == 0)
            {
                item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            }
            ui->tableWidget->setItem(i, j, item);
        }
    }
    ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget->blockSignals(oldState);
}

employList::~employList()
{
    delete ui;
}


void employList::on_pushButton_2_clicked()
{
    newEmployee newE;
    newE.setModal(true);
    newE.exec();
    this->hide();
    employList list;
    list.setModal(true);
    list.exec();
}

void employList::on_pushButton_clicked()
{
    this->hide();
    Admin admin;
    admin.setModal(true);
    admin.exec();
}

void employList::on_tableWidget_cellChanged(int row, int column)
{
    DataEmployee employee_base;

    std::string line = ui->tableWidget->item(row, 1)->text().toUtf8().constData();
    line = ui->tableWidget->item(row, 0)->text().toUtf8().constData();
    for (int i = 1; i < 7; i++){
        line += ":";
        line += ui->tableWidget->item(row, i)->text().toUtf8().constData();
    }
    employee_base.change(row + 1, line);

}

void employList::on_pushButton_3_clicked()
{
    DataEmployee employee_base;
    QString line = ui->lineEdit->text();
    std::string param = ui->comboBox->currentText().toUtf8().constData();
    std::vector<employeeStruct> result;
    if (param == "Логин"){
        result = employee_base.find("login", line.toUtf8().constData());
    } else if (param == "Адрес") {
        result = employee_base.find("address", line.toUtf8().constData());
    } else if (param == "Номер телефона") {
        result = employee_base.find("number", line.toUtf8().constData());
    } else if (param == "Имя") {
        result = employee_base.find("name", line.toUtf8().constData());
    } else if (param == "Дата рождения") {
        result = employee_base.find("birth", line.toUtf8().constData());
    } else if (param == "Возраст") {
        result = employee_base.find("age", line.toUtf8().constData());
    } else if (param == "Должность") {
        result = employee_base.find("job", line.toUtf8().constData());
    }
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setRowCount(result.size());
    bool oldState = ui->tableWidget->blockSignals(true);

    ui->tableWidget->setRowCount(result.size());
    ui->tableWidget->setColumnCount(7);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Логин"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Имя"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Адрес"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Номер"));
    ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Дата рождения"));
    ui->tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem("Возраст"));
    ui->tableWidget->setHorizontalHeaderItem(6, new QTableWidgetItem("Баланс"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i < result.size(); i ++){
        employeeStruct cl = result[i];
        item = new QTableWidgetItem(QString(cl.login.c_str()));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        ui->tableWidget->setItem(i, 0, item);
        item = new QTableWidgetItem(QString(cl.name.c_str()));
        ui->tableWidget->setItem(i, 1, item);
        item = new QTableWidgetItem(QString(cl.address.c_str()));
        ui->tableWidget->setItem(i, 2, item);
        item = new QTableWidgetItem(QString(cl.number.c_str()));
        ui->tableWidget->setItem(i, 3, item);
        item = new QTableWidgetItem(QString(cl.birth.c_str()));
        ui->tableWidget->setItem(i, 4, item);
        item = new QTableWidgetItem(QString::number(cl.age));
        ui->tableWidget->setItem(i, 5, item);
        item = new QTableWidgetItem(QString(cl.job.c_str()));
        ui->tableWidget->setItem(i, 6, item);
    }
    ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget->blockSignals(oldState);
}
