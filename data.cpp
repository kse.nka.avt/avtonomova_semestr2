#include "data.h"
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <regex>
#include <QString>
#include <QDebug>

std::vector<std::string> split(const std::string & stringToSplit,const std::string & regexString)
{
    std::regex regexToSplitWith {regexString};
    std::sregex_token_iterator wordsIterator {stringToSplit.begin(),stringToSplit.end(),regexToSplitWith,-1};
    return {wordsIterator,std::sregex_token_iterator{}};
}

DataTransaction::DataTransaction()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        transactionStruct from_base(words[1], stod(words[2]), words[3], words[4], words[5]);
        data.push_back(from_base);
    }
    input.close();
}

std::vector<std::string> DataTransaction::read(int index)
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если первый символ прочитанной строки равен икомому индексу
        {
            input.close();
            return words; // то строка найдена, расходимся
        }
    }
    input.close();
    return {}; // не нашли - вернем пустой
}

std::vector<std::vector<std::string>> DataTransaction::read_all()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    std::vector<std::vector<std::string>> result;
    while (std::getline(input, line))
    {
        // просто построчно пихаем в вектор, для удобства разделяем по символу
        std::vector<std::string> words = split(line,  ":");
        result.push_back(words);
    }
    input.close();
    return result;
}

void DataTransaction::change(int index, std::string change)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line; // буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если нашли нужный индекс, заменяем в буферную строку на нужную
        {
            line = std::to_string(index) + ":" + change;
        }
        line += "\n";
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

void DataTransaction::remove(int index)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line;// буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":"); // если нашли нужный индекс, то заменяем ее на пустоту
        if (words[0] == std::to_string(index))
        {
            line = "";
        } else { // а если нет, то добавляем перенос строки
            line += "\n";
        }
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

void DataTransaction::add(std::string line)
{
    std::ifstream input(name, std::fstream::binary);
    // далее начинается сильно костыльный поиск последней строки, тут черт ногу сломит, давайте закроем на это глаза?
    input.seekg(-1,std::ios_base::end);
    bool keepLooping = true;
    int flag = 0;
    while(keepLooping) {
        char ch;
        input.get(ch);
        if((int)input.tellg() <= 1) {
            input.seekg(0);
            keepLooping = false;
        }
        else if(ch == '\n') {
            if (flag == 0){
                input.seekg(-2,std::ios_base::cur);
                flag += 1;
            } else keepLooping = false;
        }
        else {
            input.seekg(-2,std::ios_base::cur);
        }
    }
    std::string lastLine;
    getline(input,lastLine); // вжух! последняя строка найдена
    std::vector<std::string> words = split(lastLine,  ":");
    int index = stoi(words[0]); // берем ее индекс и делаем его числом
    input.close();
    // открываем БД на запись
    std::fstream adding;
    adding.open(name, std::ios_base::app | std::ios_base::out);
    adding << (index + 1) << ":" << line << "\n"; // сами добавляем индекс на единицу больший, чем предыдущий, и саму переданную строку
    adding.close();
}

int DataTransaction::findFirstIndex(std::string param, std::string content){
    for (int i=0; i < data.size(); i++){
        if (param == "summ"){
            if (data[i].summ == stod(content)){
                return i;
            }
        } else if (param == "type"){
            if (data[i].type == content){
                return i;
            }
        } else if (param == "extra_info"){
            if (data[i].extra_info == content){
                return i;
            }
        } else if (param == "client_login"){
            if (data[i].client_login == content){
                return i;
            }
        } else if (param == "employee_login"){
            if (data[i].employee_login == content){
                return i;
            }
        }
    }
    return -1;
}

currencyStruct* DataCurrency::find(std::string param, std::string content){
    for (int i=0; i < data.size(); i++){
        if (param == "name"){
            if (data[i].name == content){
                return &data[i];
            }
        } else if (param == "country"){
            if (data[i].country == content){
                return &data[i];
            }
        } else if (param == "extra_info"){
            if (data[i].to_dollar == stof(content)){
                return &data[i];
            }
        }
    }
    return nullptr;
}

std::vector<transactionStruct> DataTransaction::find(std::string param, std::string content)
{
    std::vector<transactionStruct> results;
    for (int i=0; i < data.size(); i++){
        if (param == "summ"){
            if (QString::number(data[i].summ).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "type"){
            if (QString(data[i].type.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "extra_info"){
            if (QString(data[i].extra_info.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "client_login"){
            if (QString(data[i].client_login.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "employee_login"){
            if (QString(data[i].employee_login.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        }
    }
    return results;
}

DataCurrency::DataCurrency()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        currencyStruct from_base;
        from_base.name = words[1];
        from_base.country = words[2];
        from_base.to_dollar = strtof((words[3]).c_str(), 0);
        data.push_back(from_base);
    }
    input.close();
}

// читаем из файла одну строку по индексу
std::vector<std::string> DataCurrency::read(int index)
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если первый символ прочитанной строки равен икомому индексу
        {
            input.close();
            return words; // то строка найдена, расходимся
        }
    }
    input.close();
    return {}; // не нашли - вернем пустой
}

// читаем все данные
std::vector<std::vector<std::string>> DataCurrency::read_all()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    std::vector<std::vector<std::string>> result;
    while (std::getline(input, line))
    {
        // просто построчно пихаем в вектор, для удобства разделяем по символу
        std::vector<std::string> words = split(line,  ":");
        result.push_back(words);
    }
    input.close();
    return result;
}

// заменяем строку (самый костыльный метод, но я старалась)
void DataCurrency::change(int index, std::string change)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line; // буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если нашли нужный индекс, заменяем в буферную строку на нужную
        {
            line = std::to_string(index) + ":" + change;
        }
        line += "\n";
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

// удаление строки
void DataCurrency::remove(int index)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line;// буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":"); // если нашли нужный индекс, то заменяем ее на пустоту
        if (words[0] == std::to_string(index))
        {
            line = "";
        } else { // а если нет, то добавляем перенос строки
            line += "\n";
        }
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

// добавление в БД
void DataCurrency::add(std::string line)
{
    std::ifstream input(name, std::fstream::binary);
    // далее начинается сильно костыльный поиск последней строки, тут черт ногу сломит, давайте закроем на это глаза?
    input.seekg(-1,std::ios_base::end);
    bool keepLooping = true;
    int flag = 0;
    while(keepLooping) {
        char ch;
        input.get(ch);
        if((int)input.tellg() <= 1) {
            input.seekg(0);
            keepLooping = false;
        }
        else if(ch == '\n') {
            if (flag == 0){
                input.seekg(-2,std::ios_base::cur);
                flag += 1;
            } else keepLooping = false;
        }
        else {
            input.seekg(-2,std::ios_base::cur);
        }
    }
    std::string lastLine;
    getline(input,lastLine); // вжух! последняя строка найдена
    std::vector<std::string> words = split(lastLine,  ":");
    int index = stoi(words[0]); // берем ее индекс и делаем его числом
    input.close();
    // открываем БД на запись
    std::fstream adding;
    adding.open(name, std::ios_base::app | std::ios_base::out);
    adding << (index + 1) << ":" << line << "\n"; // сами добавляем индекс на единицу больший, чем предыдущий, и саму переданную строку
    adding.close();
}


DataEmployee::DataEmployee()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        employeeStruct from_base(words[7], words[2], words[3], words[4], words[5], atoi(words[6].c_str()), words[1]);
        data.push_back(from_base);
    }
    input.close();

}

unsigned int DataEmployee::findFirstIndex(std::string param, std::string content){
    for (unsigned int i=0; i < data.size(); i++){
        if (param == "login"){
            if (data[i].login == content){
                return i;
            }
        } else if (param == "name"){
            if (data[i].name == content){
                return i;
            }
        } else if (param == "address"){
            if (data[i].address == content){
                return i;
            }
        } else if (param == "number"){
            if (data[i].number == content){
                return i;
            }
        } else if (param == "birth"){
            if (data[i].birth == content){
                return i;
            }
        } else if (param == "job"){
            if (data[i].job == content){
                return i;
            }
        } else if (param == "age"){
            if (data[i].age == stoi(content)){
                return i;
            }
        }
    }
    return -1;
}

// читаем из файла одну строку по индексу
std::vector<std::string> DataEmployee::read(int index)
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если первый символ прочитанной строки равен икомому индексу
        {
            input.close();
            return words; // то строка найдена, расходимся
        }
    }
    input.close();
    return {}; // не нашли - вернем пустой
}

// читаем все данные
std::vector<std::vector<std::string>> DataEmployee::read_all()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    std::vector<std::vector<std::string>> result;
    while (std::getline(input, line))
    {
        // просто построчно пихаем в вектор, для удобства разделяем по символу
        std::vector<std::string> words = split(line,  ":");
        result.push_back(words);
    }
    input.close();
    return result;
}

std::vector<employeeStruct> DataEmployee::find(std::string param, std::string content)
{
    std::vector<employeeStruct> results;
    for (unsigned int i=0; i < data.size(); i++){

        if (param == "login"){
            if (QString(data[i].login.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "name"){
            if (QString(data[i].name.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "address"){
            if (QString(data[i].address.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "number"){
            if (QString(data[i].number.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "birth"){
            if (QString(data[i].birth.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "job"){
            if (QString(data[i].job.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "age"){
            if (QString::number(data[i].age).contains(content.c_str())){
                results.push_back(data[i]);
            }
        }
    }
    return results;
}
std::vector<clientStruct> DataClient::find(std::string param, std::string content)
{
    std::vector<clientStruct> results;
    for (unsigned int i=0; i < data.size(); i++){

        if (param == "login"){
            if (QString(data[i].login.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "name"){
            if (QString(data[i].name.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "address"){
            if (QString(data[i].address.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "number"){
            if (QString(data[i].number.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "birth"){
            if (QString(data[i].birth.c_str()).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "balance"){
            if (QString::number(data[i].balance).contains(content.c_str())){
                results.push_back(data[i]);
            }
        } else if (param == "age"){
            if (QString::number(data[i].age).contains(content.c_str())){
                results.push_back(data[i]);
            }
        }
    }
    return results;
}

// заменяем строку (самый костыльный метод, но я старалась)
void DataEmployee::change(int index, std::string change)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line; // буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если нашли нужный индекс, заменяем в буферную строку на нужную
        {
            line = std::to_string(index) + ":" + change;
        }
        line += "\n";
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

// удаление строки
void DataEmployee::remove(int index)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line;// буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":"); // если нашли нужный индекс, то заменяем ее на пустоту
        if (words[0] == std::to_string(index))
        {
            line = "";
        } else { // а если нет, то добавляем перенос строки
            line += "\n";
        }
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

// добавление в БД
void DataEmployee::add(std::string line)
{
    std::ifstream input(name, std::fstream::binary);
    // далее начинается сильно костыльный поиск последней строки, тут черт ногу сломит, давайте закроем на это глаза?
    input.seekg(-1,std::ios_base::end);
    bool keepLooping = true;
    int flag = 0;
    while(keepLooping) {
        char ch;
        input.get(ch);
        if((int)input.tellg() <= 1) {
            input.seekg(0);
            keepLooping = false;
        }
        else if(ch == '\n') {
            if (flag == 0){
                input.seekg(-2,std::ios_base::cur);
                flag += 1;
            } else keepLooping = false;
        }
        else {
            input.seekg(-2,std::ios_base::cur);
        }
    }
    std::string lastLine;
    getline(input,lastLine); // вжух! последняя строка найдена
    std::vector<std::string> words = split(lastLine,  ":");
    int index = stoi(words[0]); // берем ее индекс и делаем его числом
    input.close();
    // открываем БД на запись
    std::fstream adding;
    adding.open(name, std::ios_base::app | std::ios_base::out);
    adding << (index + 1) << ":" << line << "\n"; // сами добавляем индекс на единицу больший, чем предыдущий, и саму переданную строку
    adding.close();
}

DataClient::DataClient()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        clientStruct from_base(atof((words[7].c_str())), words[2], words[3], words[4], words[5], atoi(words[6].c_str()), words[1]);
        data.push_back(from_base);
    }
    input.close();

}

int DataClient::findFirstIndex(std::string param, std::string content){
    for (int i=0; i < data.size(); i++){
        if (param == "login"){
            if (data[i].login == content){
                return i;
            }
        } else if (param == "name"){
            if (data[i].name == content){
                return i;
            }
        } else if (param == "address"){
            if (data[i].address == content){
                return i;
            }
        } else if (param == "number"){
            if (data[i].number == content){
                return i;
            }
        } else if (param == "birth"){
            if (data[i].birth == content){
                return i;
            }
        } else if (param == "balance"){
            if (data[i].balance == stoi(content)){
                return i;
            }
        } else if (param == "age"){
            if (data[i].age == stoi(content)){
                return i;
            }
        }
    }
    return -1;
}

// читаем из файла одну строку по индексу
std::vector<std::string> DataClient::read(int index)
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если первый символ прочитанной строки равен икомому индексу
        {
            input.close();
            return words; // то строка найдена, расходимся
        }
    }
    input.close();
    return {}; // не нашли - вернем пустой
}

// читаем все данные
std::vector<std::vector<std::string>> DataClient::read_all()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    std::vector<std::vector<std::string>> result;
    while (std::getline(input, line))
    {
        // просто построчно пихаем в вектор, для удобства разделяем по символу
        std::vector<std::string> words = split(line,  ":");
        result.push_back(words);
    }
    input.close();
    return result;
}

// заменяем строку (самый костыльный метод, но я старалась)
void DataClient::change(int index, std::string change)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line; // буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если нашли нужный индекс, заменяем в буферную строку на нужную
        {
            line = std::to_string(index) + ":" + change;
        }
        line += "\n";
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

// удаление строки
void DataClient::remove(int index)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line;// буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":"); // если нашли нужный индекс, то заменяем ее на пустоту
        if (words[0] == std::to_string(index))
        {
            line = "";
        } else { // а если нет, то добавляем перенос строки
            line += "\n";
        }
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

// добавление в БД
void DataClient::add(std::string line)
{
    std::ifstream input(name, std::fstream::binary);
    // далее начинается сильно костыльный поиск последней строки, тут черт ногу сломит, давайте закроем на это глаза?
    input.seekg(-1,std::ios_base::end);
    bool keepLooping = true;
    int flag = 0;
    while(keepLooping) {
        char ch;
        input.get(ch);
        if((int)input.tellg() <= 1) {
            input.seekg(0);
            keepLooping = false;
        }
        else if(ch == '\n') {
            if (flag == 0){
                input.seekg(-2,std::ios_base::cur);
                flag += 1;
            } else keepLooping = false;
        }
        else {
            input.seekg(-2,std::ios_base::cur);
        }
    }
    std::string lastLine;
    getline(input,lastLine); // вжух! последняя строка найдена
    std::vector<std::string> words = split(lastLine,  ":");
    int index = stoi(words[0]); // берем ее индекс и делаем его числом
    input.close();
    // открываем БД на запись
    std::fstream adding;
    adding.open(name, std::ios_base::app | std::ios_base::out);
    adding << (index + 1) << ":" << line << "\n"; // сами добавляем индекс на единицу больший, чем предыдущий, и саму переданную строку
    adding.close();
}

DataLogin::DataLogin()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        loginStruct from_base(words[1], words[2]);
        data.push_back(from_base);
    }
    input.close();

}

// читаем из файла одну строку по индексу
std::vector<std::string> DataLogin::read(int index)
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если первый символ прочитанной строки равен икомому индексу
        {
            input.close();
            return words; // то строка найдена, расходимся
        }
    }
    input.close();
    return {}; // не нашли - вернем пустой
}

int DataLogin::findFirstIndex(std::string param, std::string content){
    for (int i=0; i < data.size(); i++){
        if (param == "login"){
            if (data[i].login == content){
                return i;
            }
        } else if (param == "password"){
            if (data[i].password == content){
                return i;
            }
        }
    }
    return -1;
}

// читаем все данные
std::vector<std::vector<std::string>> DataLogin::read_all()
{
    std::ifstream input(name, std::fstream::binary);
    std::string line;
    std::vector<std::vector<std::string>> result;
    while (std::getline(input, line))
    {
        // просто построчно пихаем в вектор, для удобства разделяем по символу
        std::vector<std::string> words = split(line,  ":");
        result.push_back(words);
    }
    input.close();
    return result;
}

// заменяем строку (самый костыльный метод, но я старалась)
void DataLogin::change(int index, std::string change)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line; // буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":");
        if (words[0] == std::to_string(index)) // если нашли нужный индекс, заменяем в буферную строку на нужную
        {
            line = std::to_string(index) + ":" + change;
        }
        line += "\n";
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

// удаление строки
void DataLogin::remove(int index)
{
    // открыли два файла, один - реальная БД, второй - буфер
    std::ifstream input(name, std::fstream::binary);
    std::ofstream fileout("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt"); // пришлось писать прямой путь, иначе файлик терялся
    std::string line;// буферная строка
    while (std::getline(input, line))
    {
        std::vector<std::string> words = split(line,  ":"); // если нашли нужный индекс, то заменяем ее на пустоту
        if (words[0] == std::to_string(index))
        {
            line = "";
        } else { // а если нет, то добавляем перенос строки
            line += "\n";
        }
        fileout << line; // записываем в буферный файл имеющуюся строку
    }
    input.close();
    fileout.close(); // закрыли старые файлы
    // теперь открываем реальную базу на запись, а буфер на чтение
    std::ifstream temp("/Users/ksenka/Documents/Lab2_ChangePoint/buffer.txt", std::fstream::binary);
    std::ofstream rewrite(name);
    while (std::getline(temp, line))
    {
        line += "\n";
        rewrite << line; // переписываем БД из буфера
    }
    temp.close();
    rewrite.close();
}

// добавление в БД
void DataLogin::add(std::string line)
{
    std::ifstream input(name, std::fstream::binary);
    // далее начинается сильно костыльный поиск последней строки, тут черт ногу сломит, давайте закроем на это глаза?
    input.seekg(-1,std::ios_base::end);
    bool keepLooping = true;
    int flag = 0;
    while(keepLooping) {
        char ch;
        input.get(ch);
        if((int)input.tellg() <= 1) {
            input.seekg(0);
            keepLooping = false;
        }
        else if(ch == '\n') {
            if (flag == 0){
                input.seekg(-2,std::ios_base::cur);
                flag += 1;
            } else keepLooping = false;
        }
        else {
            input.seekg(-2,std::ios_base::cur);
        }
    }
    std::string lastLine;
    getline(input,lastLine); // вжух! последняя строка найдена
    std::vector<std::string> words = split(lastLine,  ":");
    int index = stoi(words[0]); // берем ее индекс и делаем его числом
    input.close();
    // открываем БД на запись
    std::fstream adding;
    adding.open(name, std::ios_base::app | std::ios_base::out);
    adding << (index + 1) << ":" << line << "\n"; // сами добавляем индекс на единицу больший, чем предыдущий, и саму переданную строку
    adding.close();
}

