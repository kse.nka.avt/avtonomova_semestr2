#ifndef CURRENCY_H
#define CURRENCY_H
#include <string>

class currency
{
public:
    currency();
    std::string get_name();
    std::string get_country();
    float get_to_dollar();
    void set_name(std::string par);
    void set_country(std::string par);
    void set_to_dollar(float summ);
    currency load(std::string name);
private:
    std::string name;
    std::string country;
    float to_dollar;
};

#endif // CURRENCY_H
