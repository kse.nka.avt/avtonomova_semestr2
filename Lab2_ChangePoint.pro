#-------------------------------------------------
#
# Project created by QtCreator 2019-02-28T14:55:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab2_ChangePoint
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        changepoint.cpp \
    admin.cpp \
    user.cpp \
    person.cpp \
    client.cpp \
    employee.cpp \
    currency.cpp \
    data.cpp \
    clientlist.cpp \
    employeelist.cpp \
    newclient.cpp \
    newemployee.cpp \
    employlist.cpp \
    transaction.cpp \
    transactionlist.cpp \
    credit.cpp \
    exchange.cpp \
    changelogpass.cpp \
    changelogpassadmin.cpp

HEADERS += \
        changepoint.h \
    admin.h \
    user.h \
    person.h \
    client.h \
    employee.h \
    currency.h \
    data.h \
    clientlist.h \
    employeelist.h \
    newclient.h \
    newemployee.h \
    employlist.h \
    transaction.h \
    transactionlist.h \
    credit.h \
    exchange.h \
    changelogpass.h \
    changelogpassadmin.h

FORMS += \
        changepoint.ui \
    admin.ui \
    admin.ui \
    user.ui \
    clientlist.ui \
    employeelist.ui \
    newclient.ui \
    newemployee.ui \
    employlist.ui \
    transaction.ui \
    transactionlist.ui \
    credit.ui \
    exchange.ui \
    changelogpass.ui \
    changelogpassadmin.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    data \
    clients.txt \
    currency.txt \
    employees.txt \
    buffer.txt \
    login.txt \
    transactions.txt
