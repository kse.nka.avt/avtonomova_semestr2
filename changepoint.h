#ifndef CHANGEPOINT_H
#define CHANGEPOINT_H

#include <QMainWindow>
#include "admin.h"
#include "user.h"
namespace Ui {
class ChangePoint;
}

class ChangePoint : public QMainWindow
{
    Q_OBJECT

public:
    explicit ChangePoint(QWidget *parent = nullptr);
    ~ChangePoint();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ChangePoint *ui;
};

#endif // CHANGEPOINT_H
