#include "emplist.h"
#include "ui_employeelist.h"
#include "QStandardItemModel"
#include "data.h"
#include "admin.h"
#include "newemployee.h"

empList::empList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::employeeList)
{
    ui->setupUi(this);
    DataEmployee employee_database;
    std::vector<std::vector<std::string>> employees = employee_database.read_all();
    /*
    ui->tableWidget->setRowCount(employees.size());
    ui->tableWidget->setColumnCount(7);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Логин"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Имя"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Адрес"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Номер"));
    ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Дата рождения"));
    ui->tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem("Возраст"));
    ui->tableWidget->setHorizontalHeaderItem(6, new QTableWidgetItem("Должность"));
    for (unsigned int i = 0; i <= employees.size()-1; i ++){
        for (unsigned int j = 0; j <= employees[0].size()-2; j ++){
            std::string cl = employees[i][j+1];
            ui->tableWidget->setItem(i, j, new QTableWidgetItem(QString(cl.c_str())));
        }
    }
    ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    */
    /*
    QStandardItemModel *model = new QStandardItemModel;
    QStandardItem *item;

    //Заголовки столбцов
    QStringList horizontalHeader;
    horizontalHeader.append("Логин");
    horizontalHeader.append("Имя");
    horizontalHeader.append("Адрес");
    horizontalHeader.append("Номер");
    horizontalHeader.append("Дата рождения");
    horizontalHeader.append("Возраст");
    horizontalHeader.append("Должность");

    model->setHorizontalHeaderLabels(horizontalHeader);

    //Первый ряд
    for (unsigned int i = 0; i <= employees.size()-1; i ++){
        for (unsigned int j = 0; j <= employees[0].size()-2; j ++){
            std::string cl = employees[i][j+1];
            item = new QStandardItem(QString(cl.c_str()));
            model->setItem(i, j, item);
        }
    }
    ui->tableView->setModel(model);

    ui->tableView->resizeRowsToContents();
    ui->tableView->resizeColumnsToContents();
    */
}

empList::~empList()
{
    delete ui;
}
/*
void empList::on_pushButton_clicked()
{
    this->hide();
    Admin admin;
    admin.setModal(true);
    admin.exec();
}

void empList::on_pushButton_2_clicked()
{
    newEmployee newE;
    newE.setModal(true);
    newE.exec();
    this->hide();
    employeeList list;
    list.setModal(true);
    list.exec();
}
*/
