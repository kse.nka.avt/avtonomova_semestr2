#ifndef USER_H
#define USER_H

#include <QDialog>
#include "data.h"

namespace Ui {
class User;
}

class User : public QDialog
{
    Q_OBJECT

public:
    explicit User(QWidget *parent = nullptr, clientStruct *the_user = nullptr);
    explicit User(clientStruct *the_user = nullptr);
    ~User();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::User *ui;
    clientStruct *_user;
};

#endif // USER_H
