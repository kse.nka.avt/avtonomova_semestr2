#ifndef EMPLOYEELIST_H
#define EMPLOYEELIST_H

#include <QDialog>

namespace Ui {
class employeeList;
}

class employeeList : public QDialog
{
    Q_OBJECT

public:
    explicit employeeList(QWidget *parent = nullptr);
    ~employeeList();

private slots:
    //void on_pushButton_clicked();

    //void on_pushButton_2_clicked();
    //void on_tableWidget_itemChanged(QTableWidgetItem *item);

private:
    Ui::employeeList *ui;
};

#endif // EMPLOYEELIST_H
