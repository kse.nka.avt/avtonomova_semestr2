#include "credit.h"
#include "ui_credit.h"
#include "QDoubleValidator"
#include "user.h"
#include <QMessageBox>

credit::credit(QWidget *parent, clientStruct *user) :
    QDialog(parent),
    ui(new Ui::credit),
    _user(user)
{
    ui->setupUi(this);
}

credit::credit(clientStruct *user) :
    ui(new Ui::credit),
    _user(user),
    _index(-1)
{
    ui->setupUi(this);
    ui->lineEdit_4->setValidator( new QDoubleValidator(0, 100000000, 10, this) );
    DataEmployee employee_base_transactors;

    employee_base_transactors.data = employee_base_transactors.find("job", "creditor");

    int index_index = rand() % (int) employee_base_transactors.data.size();

    _index = index_index;

    ui->label_5->setText("Кредит выдает " + QString::fromUtf8(employee_base_transactors.data[index_index].name.c_str()));

}

credit::~credit()
{
    delete ui;
}

void credit::on_pushButton_clicked()
{
    this->hide();
    User user(_user);
    user.setModal(true);
    user.exec();
}

void credit::on_pushButton_3_clicked()
{
    DataClient client_base;
    double summ = ui->lineEdit_4->text().toDouble();
    _user->balance += summ;
    std::string buff;
    int index = client_base.findFirstIndex("login", _user->login);
    client_base.change(index + 1, _user->login + ":" + _user->name + ":" + _user->address + ":" + _user->number + ":" + _user->birth + ":" + std::to_string(_user->age) + ":" + std::to_string(_user->balance));
    QMessageBox::information(this, "success", "Кредит успешно выдан!");
    DataTransaction transaction_base;
    DataEmployee employee_base;
    transaction_base.add("transaction:" + std::to_string(summ) + ":" + _user->login + ":-:" + employee_base.data[_index].login);
    this->hide();
    User user(_user);
    user.setModal(true);
    user.exec();
}
