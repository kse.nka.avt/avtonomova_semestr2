#ifndef CLIENT_H
#define CLIENT_H
#include <string>
#include <vector>
#include "person.h"

class client : public person
{
public:

    client();
    client(std::string name, int age, std::string birth, std::string address, std::string number);
    float get_balance();
    //void set_balance(float par);
    void add_operation(std::string par);
    client load_client(std::string login);
    void send_money(float summ, std::string login);
    void exchange(float summ, std::string name);
    void get_credit(float summ);
private:
    float balance = 0;
    std::vector<std::string> operations = {};
};

#endif // CLIENT_H
