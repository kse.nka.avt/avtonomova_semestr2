#include "transactionlist.h"
#include "ui_transactionlist.h"
#include "admin.h"
#include "data.h"

transactionList::transactionList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::transactionList)
{
    ui->setupUi(this);
    DataTransaction transaction_database;
    std::vector<std::vector<std::string>> transactions = transaction_database.read_all();
    bool oldState = ui->tableWidget->blockSignals(true);

    ui->tableWidget->setRowCount(transactions.size());
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Операция"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Сумма"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Отправитель"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Дополнительная информация\n(получатель/валюта)"));
    ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Сотрудник"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i <= transactions.size()-1; i ++){
        for (unsigned int j = 0; j <= transactions[0].size()-2; j ++){
            std::string cl = transactions[i][j+1];
            item = new QTableWidgetItem(QString(cl.c_str()));
            if (j == 0)
            {
                item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            }
            ui->tableWidget->setItem(i, j, item);
        }
    }
    ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget->blockSignals(oldState);
}

transactionList::~transactionList()
{
    delete ui;
}

void transactionList::on_pushButton_clicked()
{
    this->hide();
    Admin admin;
    admin.setModal(true);
    admin.exec();
}

void transactionList::on_tableWidget_cellChanged(int row, int column)
{
    DataTransaction transaction_base;

    std::string line = ui->tableWidget->item(row, 1)->text().toUtf8().constData();
    line = ui->tableWidget->item(row, 0)->text().toUtf8().constData();
    for (int i = 1; i < 5; i++){
        line += ":";
        line += ui->tableWidget->item(row, i)->text().toUtf8().constData();
    }
    transaction_base.change(row + 1, line);

}

void transactionList::on_pushButton_3_clicked()
{
    DataTransaction transaction_base;
    QString line = ui->lineEdit->text();
    std::string param = ui->comboBox->currentText().toUtf8().constData();
    std::vector<transactionStruct> result;
    if (param == "Операция"){
        result = transaction_base.find("type", line.toUtf8().constData());
    } else if (param == "Сумма") {
        result = transaction_base.find("summ", line.toUtf8().constData());
    } else if (param == "Отправитель") {
        result = transaction_base.find("client_login", line.toUtf8().constData());
    } else if (param == "Доп. информация") {
        result = transaction_base.find("extra_info", line.toUtf8().constData());
    } else if (param == "Сотрудник") {
        result = transaction_base.find("employee_login", line.toUtf8().constData());
    }
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setRowCount(result.size());
    bool oldState = ui->tableWidget->blockSignals(true);

    ui->tableWidget->setRowCount(result.size());
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Операция"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Сумма"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Отправитель"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Дополнительная информация\n(получатель/валюта)"));
    ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Сотрудник"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i < result.size(); i ++){
        transactionStruct cl = result[i];
        item = new QTableWidgetItem(QString(cl.type.c_str()));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        ui->tableWidget->setItem(i, 0, item);
        item = new QTableWidgetItem(QString::number(cl.summ));
        ui->tableWidget->setItem(i, 1, item);
        item = new QTableWidgetItem(QString(cl.client_login.c_str()));
        ui->tableWidget->setItem(i, 2, item);
        item = new QTableWidgetItem(QString(cl.extra_info.c_str()));
        ui->tableWidget->setItem(i, 3, item);
        item = new QTableWidgetItem(QString(cl.employee_login.c_str()));
        ui->tableWidget->setItem(i, 4, item);
    }
    ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget->blockSignals(oldState);
}
