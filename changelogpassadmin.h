#ifndef CHANGELOGPASSADMIN_H
#define CHANGELOGPASSADMIN_H

#include <QDialog>

namespace Ui {
class ChangeLogPassAdmin;
}

class ChangeLogPassAdmin : public QDialog
{
    Q_OBJECT

public:
    explicit ChangeLogPassAdmin(QWidget *parent = nullptr);
    explicit ChangeLogPassAdmin(std::string login);
    ~ChangeLogPassAdmin();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::ChangeLogPassAdmin *ui;
    std::string _login;
};

#endif // CHANGELOGPASSADMIN_H
