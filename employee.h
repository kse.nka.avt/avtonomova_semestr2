#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include <string>
#include <vector>
#include "person.h"


class employee : public person
{
public:
    employee();
    std::string get_job();
    void set_job();
    employee load(std::string login);
private:
    std::string job;
};

#endif // EMPLOYEE_H
