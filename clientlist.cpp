#include "clientlist.h"
#include "ui_clientlist.h"
#include "data.h"
#include "QStandardItemModel"
#include "admin.h"
#include "newclient.h"
#include <QDebug>

clientList::clientList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::clientList)
{
    ui->setupUi(this);
    DataClient client_database;
    std::vector<std::vector<std::string>> clients = client_database.read_all();
    bool oldState = ui->tableWidget->blockSignals(true);

    ui->tableWidget->setRowCount(clients.size());
    ui->tableWidget->setColumnCount(7);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Логин"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Имя"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Адрес"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Номер"));
    ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Дата рождения"));
    ui->tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem("Возраст"));
    ui->tableWidget->setHorizontalHeaderItem(6, new QTableWidgetItem("Баланс"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i <= clients.size()-1; i ++){
        for (unsigned int j = 0; j <= clients[0].size()-2; j ++){
            std::string cl = clients[i][j+1];
            item = new QTableWidgetItem(QString(cl.c_str()));
            if (j == 0)
            {
                item->setFlags(item->flags() ^ Qt::ItemIsEditable);
            }
            ui->tableWidget->setItem(i, j, item);
        }
    }
    ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget->blockSignals(oldState);
    /*
    QStandardItemModel *model = new QStandardItemModel;
    QStandardItem *item;

    //Заголовки столбцов
    QStringList horizontalHeader;
    horizontalHeader.append("Логин");
    horizontalHeader.append("Имя");
    horizontalHeader.append("Адрес");
    horizontalHeader.append("Номер");
    horizontalHeader.append("Дата рождения");
    horizontalHeader.append("Возраст");
    horizontalHeader.append("Баланс");

    model->setHorizontalHeaderLabels(horizontalHeader);

    //Первый ряд
    for (unsigned int i = 0; i <= clients.size()-1; i ++){
        for (unsigned int j = 0; j <= clients[0].size()-2; j ++){
            std::string cl = clients[i][j+1];
            item = new QStandardItem(QString(cl.c_str()));
            model->setItem(i, j, item);
        }
    }
    ui->tableView->setModel(model);

    ui->tableView->resizeRowsToContents();
    ui->tableView->resizeColumnsToContents();
    */
}

clientList::~clientList()
{
    delete ui;
}

void clientList::on_pushButton_clicked()
{
    this->hide();
    Admin admin;
    admin.setModal(true);
    admin.exec();
}

void clientList::on_pushButton_2_clicked()
{
    newClient add;
    add.setModal(true);
    add.exec();
    clientList list;
    list.setModal(true);
    list.exec();
}



void clientList::on_tableWidget_cellChanged(int row, int column)
{
    DataClient client_base;
    std::string line = ui->tableWidget->item(row, 1)->text().toUtf8().constData();
    line = ui->tableWidget->item(row, 0)->text().toUtf8().constData();
    for (int i = 1; i < 7; i++){
        line += ":";
        line += ui->tableWidget->item(row, i)->text().toUtf8().constData();
    }
    client_base.change(row + 1, line);
}

void clientList::on_pushButton_3_clicked()
{
    DataClient client_base;
    QString line = ui->lineEdit->text();
    std::string param = ui->comboBox->currentText().toUtf8().constData();
    std::vector<clientStruct> result;
    if (param == "Логин"){
        result = client_base.find("login", line.toUtf8().constData());
    } else if (param == "Адрес") {
        result = client_base.find("address", line.toUtf8().constData());
    } else if (param == "Номер телефона") {
        result = client_base.find("number", line.toUtf8().constData());
    } else if (param == "Имя") {
        result = client_base.find("name", line.toUtf8().constData());
    } else if (param == "Дата рождения") {
        result = client_base.find("birth", line.toUtf8().constData());
    } else if (param == "Возраст") {
        result = client_base.find("age", line.toUtf8().constData());
    } else if (param == "Баланс") {
        result = client_base.find("balance", line.toUtf8().constData());
    }
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setRowCount(result.size());
    bool oldState = ui->tableWidget->blockSignals(true);

    ui->tableWidget->setRowCount(result.size());
    ui->tableWidget->setColumnCount(7);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Логин"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Имя"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Адрес"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Номер"));
    ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem("Дата рождения"));
    ui->tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem("Возраст"));
    ui->tableWidget->setHorizontalHeaderItem(6, new QTableWidgetItem("Баланс"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i < result.size(); i ++){
        clientStruct cl = result[i];
        item = new QTableWidgetItem(QString(cl.login.c_str()));
        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
        ui->tableWidget->setItem(i, 0, item);
        item = new QTableWidgetItem(QString(cl.name.c_str()));
        ui->tableWidget->setItem(i, 1, item);
        item = new QTableWidgetItem(QString(cl.address.c_str()));
        ui->tableWidget->setItem(i, 2, item);
        item = new QTableWidgetItem(QString(cl.number.c_str()));
        ui->tableWidget->setItem(i, 3, item);
        item = new QTableWidgetItem(QString(cl.birth.c_str()));
        ui->tableWidget->setItem(i, 4, item);
        item = new QTableWidgetItem(QString::number(cl.age));
        ui->tableWidget->setItem(i, 5, item);
        item = new QTableWidgetItem(QString::number(cl.balance));
        ui->tableWidget->setItem(i, 6, item);
    }
    ui->tableWidget->resizeRowsToContents();
    ui->tableWidget->resizeColumnsToContents();
    ui->tableWidget->blockSignals(oldState);
}
