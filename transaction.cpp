#include "transaction.h"
#include "ui_transaction.h"
#include "data.h"
#include <random>
#include <QtDebug>
#include "user.h"
#include<QMessageBox>
#include <QDoubleValidator>

transaction::transaction(QWidget *parent, clientStruct *the_user) :
    QDialog(parent),
    ui(new Ui::transaction),
    _user(the_user),
    _index(-1)
{
    ui->setupUi(this);
}

transaction::~transaction()
{
    delete ui;
}

transaction::transaction(clientStruct *the_user) :
    ui(new Ui::transaction),
    _user(the_user),
    _index(-1)
{
    ui->setupUi(this);
    ui->lineEdit_2->setValidator( new QDoubleValidator(0, 100000000, 10, this) );
    DataEmployee employee_base_transactors;

    employee_base_transactors.data = employee_base_transactors.find("job", "transactor");

    int index_index = rand() % (int) employee_base_transactors.data.size();

    _index = index_index;

    ui->label->setText("Перевод выполняет " + QString::fromUtf8(employee_base_transactors.data[index_index].name.c_str()));

}

void transaction::on_pushButton_2_clicked()
{
    this->hide();
    User user(_user);
    user.setModal(true);
    user.exec();
}

void transaction::on_pushButton_clicked()
{
    DataClient client_base;
    if (client_base.find("login", ui->lineEdit->text().toUtf8().constData()).size() == 0)
    {
        QMessageBox::warning(this, "error", "Ошибка! Неверный логин получателя");
    } else
    {
        double summ = ui->lineEdit_2->text().toDouble();
        if (summ > _user->balance)
        {
            QMessageBox::warning(this, "error", "Ошибка! Недостаточно средств");
        } else
        {
            _user->balance -= summ;
            std::string buff;
            int index = client_base.findFirstIndex("login", _user->login);
            client_base.change(index + 1, _user->login + ":" + _user->name + ":" + _user->address + ":" + _user->number + ":" + _user->birth + ":" + std::to_string(_user->age) + ":" + std::to_string(_user->balance));
            index = client_base.findFirstIndex("login", ui->lineEdit->text().toUtf8().constData());
            clientStruct recivier = client_base.data[index];
            client_base.change(index + 1, recivier.login + ":" + recivier.name + ":" + recivier.address + ":" + recivier.number + ":" + recivier.birth + ":" + std::to_string(recivier.age) + ":" + std::to_string(recivier.balance + summ));
            QMessageBox::information(this, "success", "Перевод выполнен успешно!");
            DataTransaction transaction_base;
            DataEmployee employee_base;
            transaction_base.add("transaction:" + std::to_string(summ) + ":" + _user->login + ":" + ui->lineEdit->text().toUtf8().constData() + ":" + employee_base.data[_index].login);
            this->hide();
            User user(_user);
            user.setModal(true);
            user.exec();
        }
    }

}
