#include "admin.h"
#include "ui_admin.h"
#include "data.h"
#include "clientlist.h"
#include "employlist.h"
#include "transactionlist.h"
#include "changelogpassadmin.h"

Admin::Admin(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Admin)
{
    ui->setupUi(this);
}

Admin::Admin(std::string login) :
    ui(new Ui::Admin),
    _login(login)
{
    ui->setupUi(this);
}

Admin::~Admin()
{
    delete ui;
}

void Admin::on_pushButton_2_clicked()
{
    this->hide();
    clientList list;
    list.setModal(true);
    list.exec();

}

void Admin::on_pushButton_clicked()
{
    this->hide();
    employList list;
    list.setModal(true);
    list.exec();
}

void Admin::on_pushButton_3_clicked()
{
    this->hide();
    transactionList list;
    list.setModal(true);
    list.exec();
}

void Admin::on_pushButton_4_clicked()
{
    this->hide();
    ChangeLogPassAdmin change(_login);
    change.setModal(true);
    change.exec();
}
