#include "changelogpass.h"
#include "ui_changelogpass.h"
#include "user.h"
#include <QMessageBox>
#include <QDebug>


ChangeLogPass::ChangeLogPass(QWidget *parent, clientStruct *user) :
    QDialog(parent),
    ui(new Ui::ChangeLogPass),
    _user(user)
{
    ui->setupUi(this);
}

ChangeLogPass::ChangeLogPass(clientStruct *user) :
    ui(new Ui::ChangeLogPass),
    _user(user)
{
    qDebug() << QString(_user->login.c_str());
    ui->setupUi(this);
}

ChangeLogPass::~ChangeLogPass()
{
    delete ui;
}

void ChangeLogPass::on_pushButton_clicked()
{
    this->hide();
    User user(_user);
    user.setModal(true);
    user.exec();
}

void ChangeLogPass::on_pushButton_2_clicked()
{
    std::string new_login = ui->lineEdit->text().toUtf8().constData();
    std::string new_password = ui->lineEdit_2->text().toUtf8().constData();

    DataClient client_base;
    int index_client_base = client_base.findFirstIndex("login", _user->login);
    DataLogin login_base;
    int index_login_base = login_base.findFirstIndex("login", _user->login);
    _user->login = new_login;
    login_base.change(index_login_base + 1, new_login + ":" + new_password + ":user");
    client_base.change(index_client_base + 1, new_login + ":" + _user->name + ":" + _user->address + ":" + _user->number + ":" + _user->birth + ":" + std::to_string(_user->age) + ":" + std::to_string(_user->balance));
    QMessageBox::information(this, "succes", "Пароль и логин успешно изменен.");
    this->hide();
    User user(_user);
    user.setModal(true);
    user.exec();
}
