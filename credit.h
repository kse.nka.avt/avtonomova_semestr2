#ifndef CREDIT_H
#define CREDIT_H

#include <QDialog>
#include "data.h"

namespace Ui {
class credit;
}

class credit : public QDialog
{
    Q_OBJECT

public:
    explicit credit(QWidget *parent = nullptr, clientStruct *user = nullptr);
    explicit credit(clientStruct *user = nullptr);
    ~credit();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::credit *ui;
    clientStruct *_user;
    int _index;
};

#endif // CREDIT_H
