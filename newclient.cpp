#include "newclient.h"
#include "ui_newclient.h"
#include "data.h"

newClient::newClient(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::newClient)
{
    ui->setupUi(this);
}

newClient::~newClient()
{
    delete ui;
}

void newClient::on_pushButton_clicked()
{
    std::string login = ui->lineEdit->text().toUtf8().constData();
    std::string password = ui->lineEdit_2->text().toUtf8().constData();
    std::string name = ui->lineEdit_3->text().toUtf8().constData();
    std::string address = ui->lineEdit_4->text().toUtf8().constData();
    std::string number = ui->lineEdit_5->text().toUtf8().constData();
    std::string birth = ui->lineEdit_6->text().toUtf8().constData();
    std::string age = ui->lineEdit_7->text().toUtf8().constData();
    DataClient client_database;
    client_database.add(login + ":" + name + ":" + address + ":" + number + ":" + birth + ":" + age + ":" + "0");
    DataLogin login_database;
    login_database.add(login + ":" + password + ":user");
    this->hide();
}
