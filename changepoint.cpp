#include "changepoint.h"
#include "ui_changepoint.h"
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <QMessageBox>
#include "admin.h"
#include "user.h"
#include "data.h"
#include <QDebug>

std::vector<std::string> splitC(const std::string & stringToSplit,const std::string & regexString)
{
    std::regex regexToSplitWith {regexString};
    std::sregex_token_iterator wordsIterator {stringToSplit.begin(),stringToSplit.end(),regexToSplitWith,-1};
    return {wordsIterator,std::sregex_token_iterator{}};
}

ChangePoint::ChangePoint(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ChangePoint)
{
    ui->setupUi(this);
}

ChangePoint::~ChangePoint()
{
    delete ui;
}

void ChangePoint::on_pushButton_clicked()
{
    QString login = ui->lineEdit->text();
    QString password = ui->lineEdit_2->text();
    std::string login_s = login.toUtf8().constData();
    std::string password_s = password.toUtf8().constData();
    std::ifstream input("/Users/ksenka/Documents/Lab2_ChangePoint/login.txt", std::fstream::binary);
    std::string line;
    bool flag = false;
    std::vector<std::string> logpass;
    while (std::getline(input, line))
    {
        logpass = splitC(line, ":");
        if (logpass[1] == login_s && logpass[2] == password_s && logpass[3] == "admin"){
            flag = true;
            this->hide();
            Admin admin(login_s);
            admin.setModal(true);
            admin.exec();
            break;
        } else if (logpass[1] == login_s && logpass[2] == password_s && logpass[3] == "user"){
            DataClient client_database;
            int index = client_database.findFirstIndex("login", login_s);
            clientStruct *client = &client_database.data[index];

            flag = true;
            this->hide();
            User user(client);
            user.setModal(true);
            user.exec();
            break;
        }

    }
    if (!flag){

        QMessageBox::warning(this, "login", "wrong password or login!");
    }

}
