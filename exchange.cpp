#include "exchange.h"
#include "ui_exchange.h"
#include "data.h"
#include "user.h"

exchange::exchange(QWidget *parent, clientStruct *user) :
    QDialog(parent),
    ui(new Ui::exchange),
    _user(user)
{
    ui->setupUi(this);
    DataCurrency cur_database;
    for (unsigned int i = 0; i < cur_database.data.size(); i++)
    {
        ui->comboBox->addItem(QString(cur_database.data[i].name.c_str()));
        ui->comboBox_2->addItem(QString(cur_database.data[i].name.c_str()));
    }
}

exchange::exchange(clientStruct *user) :
    ui(new Ui::exchange),
    _user(user)
{
    ui->setupUi(this);
    DataCurrency cur_database;
    for (unsigned int i = 0; i < cur_database.data.size(); i++)
    {
        ui->comboBox->addItem(QString(cur_database.data[i].name.c_str()));
        ui->comboBox_2->addItem(QString(cur_database.data[i].name.c_str()));
        DataEmployee employee_base_transactors;

        employee_base_transactors.data = employee_base_transactors.find("job", "creditor");

        int index_index = rand() % (int) employee_base_transactors.data.size();

        ui->label_4->setText("Вас обслуживает " + QString::fromUtf8(employee_base_transactors.data[index_index].name.c_str()));
    }
}

exchange::~exchange()
{
    delete ui;
}

void exchange::on_pushButton_2_clicked()
{
    double summ = ui->lineEdit->text().toDouble();
    std::string from = ui->comboBox->currentText().toUtf8().constData();
    std::string to = ui->comboBox_2->currentText().toUtf8().constData();
    DataCurrency cur_database;
    double from_f = cur_database.find("name", from)->to_dollar;
    double to_f = cur_database.find("name", to)->to_dollar;
    double result = (1 / to_f) * from_f * summ;
    ui->label_5->setNum(result);
}

void exchange::on_pushButton_clicked()
{
    this->hide();
    User user(_user);
    user.setModal(true);
    user.exec();
}
