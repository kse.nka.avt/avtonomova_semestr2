#ifndef CLIENTLIST_H
#define CLIENTLIST_H

#include <QDialog>

namespace Ui {
class clientList;
}

class clientList : public QDialog
{
    Q_OBJECT

public:
    explicit clientList(QWidget *parent = nullptr);
    ~clientList();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_tableWidget_cellChanged(int row, int column);

    void on_pushButton_3_clicked();

private:
    Ui::clientList *ui;
};

#endif // CLIENTLIST_H
