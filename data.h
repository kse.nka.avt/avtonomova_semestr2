#ifndef DATA_H
#define DATA_H
#include <string>
#include <vector>

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <regex>

struct clientStruct{
    double balance;
    std::string name;
    std::string address;
    std::string number;
    std::string birth;
    int age;
    std::string login;
    clientStruct(double balance_p,
           std::string name_p,
           std::string address_p,
           std::string number_p,
           std::string birth_p,
           int age_p,
           std::string login_p):
            balance(balance_p),
            name(name_p),
            address(address_p),
            number(number_p),
            birth(birth_p),
            age(age_p),
            login(login_p){}
};

struct loginStruct{
    std::string login;
    std::string password;
    loginStruct(
            std::string login_p,
            std::string password_p):

            login(login_p),
    password(password_p){}
};

struct employeeStruct{
    std::string job;
    std::string name;
    std::string address;
    std::string number;
    std::string birth;
    int age;
    std::string login;
    employeeStruct(std::string job_p,
             std::string name_p,
             std::string address_p,
             std::string number_p,
             std::string birth_p,
             int age_p,
             std::string login_p):
    job(job_p),
    name(name_p),
    address(address_p),
    number(number_p),
    birth(birth_p),
    age(age_p),
    login(login_p){}
};

struct transactionStruct{
    std::string type;
    double summ;
    std::string client_login;
    std::string extra_info;
    std::string employee_login;
    transactionStruct(std::string type_p,
                    double summ_p,
                    std::string client_login_p,
                    std::string extra_info_p,
                    std::string employee_login_p):
        type(type_p),
        summ(summ_p),
        client_login(client_login_p),
        extra_info(extra_info_p),
        employee_login(employee_login_p){}
};

struct currencyStruct{
    std::string name;
    std::string country;
    double to_dollar;
};

class DataEmployee
{
public:
    DataEmployee();
    DataEmployee(std::string name);
    void add(std::string line);
    void remove(int index);
    void change(int index, std::string change);
    std::vector<std::string> read(int index);
    std::vector<std::vector<std::string>> read_all();
    std::vector<employeeStruct> data;
    unsigned findFirstIndex(std::string param, std::string content);
    std::vector<employeeStruct> find(std::string param, std::string content);
private:
    std::string name = "/Users/ksenka/Documents/Lab2_ChangePoint/employees.txt";

};


class DataCurrency
{
public:
    DataCurrency();
    DataCurrency(std::string name);
    void add(std::string line);
    void remove(int index);
    void change(int index, std::string change);
    std::vector<std::string> read(int index);
    std::vector<std::vector<std::string>> read_all();
    std::vector<currencyStruct> data;
    currencyStruct* find(std::string param, std::string content);
private:
    std::string name = "/Users/ksenka/Documents/Lab2_ChangePoint/currency.txt";

};


class DataClient
{
public:
    DataClient();
    DataClient(std::string name);
    void add(std::string line);
    void remove(int index);
    void change(int index, std::string change);
    std::vector<std::string> read(int index);
    std::vector<std::vector<std::string>> read_all();
    std::vector<clientStruct> data;
    int findFirstIndex(std::string param, std::string content);
    std::vector<clientStruct> find(std::string param, std::string content);
private:
    std::string name = "/Users/ksenka/Documents/Lab2_ChangePoint/clients.txt";

};

class DataTransaction
{
public:
    DataTransaction();
    DataTransaction(std::string name);
    void add(std::string line);
    void remove(int index);
    void change(int index, std::string change);
    std::vector<std::string> read(int index);
    std::vector<std::vector<std::string>> read_all();
    std::vector<transactionStruct> data;
    int findFirstIndex(std::string param, std::string content);
    std::vector<transactionStruct> find(std::string param, std::string content);
private:
    std::string name = "/Users/ksenka/Documents/Lab2_ChangePoint/transactions.txt";

};


class DataLogin
{
public:
    DataLogin();
    DataLogin(std::string name);
    void add(std::string line);
    void remove(int index);
    void change(int index, std::string change);
    std::vector<std::string> read(int index);
    std::vector<std::vector<std::string>> read_all();
    std::vector<loginStruct> data;
    int findFirstIndex(std::string param, std::string content);
private:
    std::string name = "/Users/ksenka/Documents/Lab2_ChangePoint/login.txt";

};

#endif // DATA_H
